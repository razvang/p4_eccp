import java.util.*;
import static java.lang.System.out;

public class Problema4 {

    private static Scanner sc;
    private static List<Double> list;
    private static double avg, dev;

    private interface Processor<K extends Double> {
        static void print(Long element) {
            out.println(element);
        }
        K calculate(List<K> numbers);
    }
    private enum STATISTICS implements Processor<Double> {
        AVG() {
            @Override
            public Double calculate(List<Double> numbers) {
                double total = numbers.stream()
                        .mapToDouble(Double::doubleValue)
                        .sum();
                return total / numbers.size();
            }
        },
        STD_DEV() {
            @Override
            public Double calculate(List<Double> numbers) {
                double standardDeviation = 0.0, total = numbers.size();

                double avg = STATISTICS.AVG.calculate(numbers);

                for (double num : numbers) {
                    standardDeviation += Math.pow(num - avg, 2);
                }

                return Math.sqrt(standardDeviation / total);
            }
        }
    }

    static {
        sc = new Scanner(System.in);
        list = new ArrayList<>();
    }

    public static void main(String... args) {

        long removed = 0L;
        double removable;

        //Nu este utila prima linie
        sc.nextLine();

        String[] line = sc.nextLine().split(" ");

        for (String el : line) {
            list.add(Double.parseDouble(el));
        }

        avg = STATISTICS.AVG.calculate(list);
        dev = STATISTICS.STD_DEV.calculate(list);

        while (dev / avg > 0.1) {
            removable = list.stream()
                            .map(value -> value - avg)
                            .max(Comparator.comparing(Math::abs))
                            .orElse(0.0);

            removed++;
            removable = Math.round(removable + avg);

            list.remove(removable);

            avg = STATISTICS.AVG.calculate(list);
            dev = STATISTICS.STD_DEV.calculate(list);
        }

        Processor.print(removed);
    }
}
